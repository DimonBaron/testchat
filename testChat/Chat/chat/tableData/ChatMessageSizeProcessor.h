//
//  ChatMessageSizeProcessor.h
//  testChat
//
//  Created by Dmitry Avvakumov on 17.10.16.
//  Copyright © 2016 Dmitry Avvakumov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class ChatMessageEntity;

@interface ChatMessageSizeProcessor : NSObject

@property (strong, nonatomic) ChatMessageEntity *item;
@property (assign, nonatomic) CGFloat preferredWidth;

- (CGFloat)calculateHeight;


@end
