//
//  ChatMessageCell.h
//  testChat
//
//  Created by Dmitry Avvakumov on 31.08.16.
//  Copyright © 2016 Dmitry Avvakumov. All rights reserved.
//

#import "ChatBaseMessageCell.h"

@class ChatMessageEntity;

@interface ChatMessageCell : ChatBaseMessageCell

- (void)configureWithItem:(ChatMessageEntity *)item;

@end
