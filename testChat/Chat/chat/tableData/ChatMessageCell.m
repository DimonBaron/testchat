//
//  ChatMessageCell.m
//  testChat
//
//  Created by Dmitry Avvakumov on 31.08.16.
//  Copyright © 2016 Dmitry Avvakumov. All rights reserved.
//

#import "ChatMessageCell.h"

// core data
#import "ChatMessageEntity.h"

@implementation ChatMessageCell

- (void)configureWithItem:(ChatMessageEntity *)item {
    
    self.messageLabel.text = item.text;
    
    if (item.isMy.boolValue) {
        [self configureSide:NO];
    } else {
        [self configureSide:YES];
    }
}

@end
