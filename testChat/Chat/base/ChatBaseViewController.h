//
//  ChatBaseViewController.h
//  testChat
//
//  Created by Dmitry Avvakumov on 31.08.16.
//  Copyright © 2016 Dmitry Avvakumov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatBaseViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) IBOutlet UILabel *textViewPlaceholderLabel;
@property (strong, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *textViewHeightConstraint;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomToKeyboardConstraint;

@property (assign, nonatomic) BOOL pagingEnabled;

#pragma mark - Data access

- (NSUInteger)allItemsCount;
- (CGFloat)heightForRowAtIndexPath:(NSIndexPath *)indexPath;

#pragma mark - Access methods

- (NSString *)modelKeyAtIndexPath:(NSIndexPath *)indexPath;
- (id)modelAtIndexPath:(NSIndexPath *)indexPath;

#pragma mark - Actions

- (IBAction)sendMessageAction:(id)sender;

@end
